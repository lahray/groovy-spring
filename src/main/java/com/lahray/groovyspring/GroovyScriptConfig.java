package com.lahray.groovyspring;

import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * File created by Lahray 16/01/2021 2:42 PM
 */

@Configuration
public class GroovyScriptConfig {

    @Bean
    public GroovyClassLoader groovyClassLoader() {
        return new GroovyClassLoader();
    }

    @Bean
    public GroovyShell groovyShell(GroovyClassLoader groovyClassLoader) {
        return new GroovyShell(groovyClassLoader, new Binding());
    }
}
