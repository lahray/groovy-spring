package com.lahray.groovyspring;

import lombok.Data;

import javax.persistence.*;

/**
 * File created by Lahray 16/01/2021 1:30 PM
 */

@Entity
@Data
public class GroovyScript {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = "TEXT NOT NULL")
    private String ruleScript;
}
