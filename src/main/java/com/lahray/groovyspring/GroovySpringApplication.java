package com.lahray.groovyspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GroovySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroovySpringApplication.class, args);
	}

}
