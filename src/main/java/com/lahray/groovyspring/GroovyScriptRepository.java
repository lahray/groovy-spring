package com.lahray.groovyspring;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * File created by Lahray 16/01/2021 1:33 PM
 */

@Repository
public interface GroovyScriptRepository extends JpaRepository<GroovyScript, Long> {


}
