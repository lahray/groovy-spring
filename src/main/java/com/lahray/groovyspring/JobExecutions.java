package com.lahray.groovyspring;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

/**
 * File created by Lahray 16/01/2021 2:18 PM
 */

@Entity
@Data
@Builder
public class JobExecutions {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDateTime lastRunTime;
}
