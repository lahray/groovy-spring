package com.lahray.groovyspring;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import groovy.lang.GroovyShell;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * File created by Lahray 16/01/2021 1:32 PM
 */

@Service
@RequiredArgsConstructor
@Slf4j
public class RuleTrigger {

    private final GroovyScriptRepository groovyScriptRepository;

    private final JobExecutionsRepository jobExecutionsRepository;

    private final GroovyClassLoader groovyClassLoader;

    private final GroovyShell groovyShell;

    @Scheduled(fixedRate = 10000L)
    public void runScript() {

        log.info("Running script!");
        List<GroovyScript> scriptList = groovyScriptRepository.findAll();

        log.info("Groovy scripts size - " + scriptList.size());

        if (scriptList.size() < 1) {
            return;
        }

        GroovyScript groovyScript = scriptList.get(0);

        String script = groovyScript.getRuleScript();

        runClass(script);
    }

    private void runClass(String groovyScript) {

        try {
            Class ruleScript = groovyClassLoader.parseClass(groovyScript);

            GroovyObject groovyClass = (GroovyObject) ruleScript.newInstance();

            groovyClass.invokeMethod("runJob", new Object[]{log, jobExecutionsRepository});
        } catch (Exception ex) {
            log.error("Error running script", ex);
        }
    }
}
