package com.lahray.groovyspring;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * File created by Lahray 16/01/2021 2:19 PM
 */

@Repository
public interface JobExecutionsRepository extends JpaRepository<JobExecutions, Long> {

}
