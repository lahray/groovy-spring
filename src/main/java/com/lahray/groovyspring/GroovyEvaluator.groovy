package com.lahray.groovyspring

import com.lahray.groovyspring.JobExecutions
import com.lahray.groovyspring.JobExecutionsRepository
import org.slf4j.Logger

import java.time.LocalDateTime

class GroovyEvaluator {

    public void runJob(Logger log, JobExecutionsRepository jobExecutionsRepository) {

        log.info("Running groovy script!");

        jobExecutionsRepository.save(JobExecutions
        .builder()
        .lastRunTime(LocalDateTime.now()).build());

        log.info("Finished running groovy script!");
    }
}
